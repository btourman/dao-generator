name := "dao-generator"

version := "0.1"

scalaVersion := "2.12.4"

mainClass in assembly := Some("Main")

libraryDependencies += "org.postgresql" % "postgresql" % "42.1.4"