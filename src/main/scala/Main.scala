import java.io.{File, PrintWriter}
import java.sql.DriverManager

import scala.collection.mutable.ListBuffer

object Main {

    def main(args : Array[String]): Unit = {
        if(args.length == 4){
        try {
            Class.forName("org.postgresql.Driver")
            val model::table::database::directory::Nil = args.toList
            val url = "jdbc:postgresql://localhost:5433/"+database
            val user = "postgres"
            val passwd = "root"
            val conn = DriverManager.getConnection(url, user, passwd)
            //Création d'un objet Statement
            val state = conn.createStatement
            //L'objet ResultSet contient le résultat de la requête SQL
            val result = state.executeQuery("SELECT * FROM "+table)
            //On récupère les MetaData
            val resultMeta = result.getMetaData
            println("\n**********************************")
            //On affiche le nom des colonnes
            val types = (1 until (resultMeta.getColumnCount + 1)).map(i => {
                (resultMeta.getColumnName(i), Class.forName(resultMeta.getColumnClassName(i)).getSimpleName)
            })
            generateCaseClass(model.capitalize, types,directory)
            generateItfDao(model.capitalize,directory)
            generateDao(model.capitalize,table,types,directory)
            result.close()
            state.close


        } catch {
            case e: Exception =>
                e.printStackTrace()
        }}else{
            println("usage: dao-gen <dao_name> <table> <database> <directory>")
        }
    }

    def generateCaseClass(name : String,
                          types: Seq[(String, String)],directory:String): Unit = {
        val pw = new PrintWriter(new File(directory+"model/"+name + ".scala"))
        val buffer = ListBuffer.empty[String]
        val attributes = types.map(t => {
            t._1 + ":" + (t._2 match {
                case "Integer" => "Int"
                case e => e
            })
        }).mkString(",")

        buffer += "package model\n"
        buffer += "import anorm.{Macro, RowParser}"
        buffer += "import play.api.libs.json.Json\n"
        buffer += "case class " + name + "(" + attributes + ")\n"

        buffer += "object " + name + " {"
        buffer += "implicit val writes = Json.writes["+name+"]"
        buffer += "val parser = Macro.indexedParser["+name+"].asInstanceOf[RowParser["+name+"]]"
        buffer += "}"

        pw.write(buffer.mkString("\n"))
        pw.close()
    }

    def generateItfDao(name:String,directory:String): Unit ={
        val pw = new PrintWriter(new File(directory+"dao/"+name + "Dao.scala"))
        val buffer = ListBuffer.empty[String]

        buffer += "package dao\n"
        buffer += "import com.google.inject.ImplementedBy"
        buffer += "import model."+name+"\n"
        buffer += "@ImplementedBy(classOf[Anorm"+name+"Dao])"
        buffer += "trait " + name + "Dao {"
        buffer += "def get(id:Long):"+name+"\n"
        buffer += "def getAll():Seq["+name+"]\n"
        buffer += "def delete(id:Long)\n"
        buffer += "def insert("+name.toLowerCase+":"+name+"):Option[Long]"
        buffer += "def update("+name.toLowerCase+":"+name+")\n"
        buffer += "}"

        pw.write(buffer.mkString("\n"))
        pw.close()
    }

    def generateDao(name:String,tableName:String,types: Seq[(String, String)],directory:String): Unit ={
        val pw = new PrintWriter(new File(directory+"dao/"+"Anorm"+name + "Dao.scala"))
        val buffer = ListBuffer.empty[String]


        buffer += "package dao\n"
        buffer += "import javax.inject.Inject"
        buffer += "import anorm._"
        buffer += "import model."+name+"\n"
        buffer += "import play.api.db.Database\n"
        buffer += "class Anorm"+name+"Dao @Inject()(implicit database: Database) extends "+name+"Dao {\n"

        generateGet(name,buffer,tableName)
        buffer += "\n"
        generateGetAll(name,buffer,tableName)
        buffer += "\n"
        generateInsert(name,buffer,types,tableName)
        buffer += "\n"
        generateDelete(name,buffer,tableName)
        buffer += "\n"
        generateUpdate(name,buffer,types,tableName)

        buffer += "}"

        pw.write(buffer.mkString("\n"))
        pw.close()
    }

    def generateGetAll(name:String,listBuffer: ListBuffer[String],tableName:String): Unit ={
        listBuffer += "def getAll(): Seq["+name+"] = {"
        generateBeginConnection(listBuffer)
        listBuffer += "SQL\"select * from "+tableName+"\".as("+name+".parser.*)"
        generateEndConnection(listBuffer)
    }
    def generateDelete(name:String,listBuffer: ListBuffer[String],tableName:String): Unit ={
        listBuffer += "def delete(id:Long): Unit = {"
        generateBeginConnection(listBuffer)
        listBuffer += "SQL\"delete from "+tableName+"where id=$id\".execute()"
        generateEndConnection(listBuffer)
    }
    def generateUpdate(name:String,listBuffer: ListBuffer[String],types:Seq[(String,String)],tableName:String): Unit ={
        val update = types.filter(!_._1.equals("id")).map(t => t._1+"="+"${"+name.toLowerCase+"."+t._1+"}").mkString(",")

        listBuffer += "def update("+name.toLowerCase+":"+name+"):Unit = {"
        generateBeginConnection(listBuffer)
        listBuffer += "SQL\"\"\"update "+tableName + "set "
        listBuffer += update
        listBuffer += "where id=${"+name.toLowerCase+".id}\"\"\".execute()"
        generateEndConnection(listBuffer)
    }
    def generateGet(name:String,listBuffer: ListBuffer[String],tableName:String): Unit ={
        listBuffer += "def get(id:Long): Option["+name+"] = {"
        generateBeginConnection(listBuffer)
        listBuffer += "SQL\"select * from "+tableName+"\".as("+name+".parser.singleOpt)"
        generateEndConnection(listBuffer)
    }
    def generateInsert(name:String,listBuffer: ListBuffer[String],types:Seq[(String,String)],tableName:String): Unit ={
        val fields = types.map(_._1).mkString(",")
        val values = types.map("${"+name.toLowerCase+"."+_._1+"}").mkString(",")
        listBuffer += "def insert("+name.toLowerCase+":"+name+"):Option[Long] = {"
        generateBeginConnection(listBuffer)
        listBuffer += "SQL\"\"\"insert into "+tableName+" ("+fields+")"
        listBuffer += "values"
        listBuffer += "("+values+")\"\"\".executeInsert()"
        generateEndConnection(listBuffer)
    }

    def generateBeginConnection(listBuffer: ListBuffer[String]): Unit ={
        listBuffer += "database.withConnection(implicit c => {"
    }

    def generateEndConnection(listBuffer: ListBuffer[String]): Unit ={
        listBuffer +="})}"
    }
}
